package com.example.layoutview_f1d016076;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class EditActivity extends AppCompatActivity {
    TextView eUser;
    TextView eNim;
    Button btnSave;
    int getPosisi;
    TextView editPosisi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit);

        eUser = findViewById(R.id.editUsername);
        eNim = findViewById(R.id.editNim);
        btnSave = findViewById(R.id.btnSave);
        editPosisi = findViewById(R.id.editPosisi);

        Intent intent = getIntent();
        getPosisi = intent.getIntExtra("posisi",0);
        String getUsername = intent.getStringExtra("name");
        String getNim = intent.getStringExtra("nim");

        editPosisi.setText("Posisi : "+getPosisi);
        eUser.setText(getUsername);
        eNim.setText(getNim);

        btnSave.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Toast.makeText(
                        getApplicationContext(),
                        "Data berhasil di perbaharui",
                        Toast.LENGTH_LONG).show();


                Intent intent1 = new Intent(getApplicationContext(),MainActivity.class);
                intent1.putExtra("name", ""+eUser.getText());
                intent1.putExtra("nim",""+eNim.getText());
                intent1.putExtra("pos",getPosisi);
                intent1.putExtra("par",1);
                startActivity(intent1);
            }
        });
    }
}
