package com.example.layoutview_f1d016076;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class item_activity extends AppCompatActivity {
    TextView dataName;
    TextView dataNim;
    TextView description;
    ImageView imgProfilView;
    int getPosisi;
    Button btnEdit;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item);

        dataName = findViewById(R.id.itemName);
        dataNim = findViewById(R.id.itemNIM);
        description = findViewById(R.id.description);
        imgProfilView = findViewById(R.id.itemProfil);
        btnEdit = findViewById(R.id.btnEdit);

        Intent intent = getIntent();
        getPosisi = intent.getIntExtra("posisi",0);
        String getDataNama = intent.getStringExtra("name");
        String getDataNim = intent.getStringExtra("nim");
        String getDesc = intent.getStringExtra("desc");
        int getDataImage = intent.getIntExtra("image",0);

        dataName.setText("Nama : "+getDataNama);
        dataNim.setText("NIM   : "+getDataNim);
        description.setText(getDesc);
        imgProfilView.setImageResource(getDataImage);

        //to send move activity and sending data
        btnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent1 = new Intent(getApplicationContext(),EditActivity.class);
                intent1.putExtra("posisi", getPosisi);
                intent1.putExtra("name", dataName.getText());
                intent1.putExtra("nim",dataNim.getText());
                startActivity(intent1);
            }
        });
        //to enable back Button
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    //getting back to ListHome
    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        if(item.getItemId() == android.R.id.home){
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }
    @Override
    public void onBackPressed(){
        super.onBackPressed();
    }
}
