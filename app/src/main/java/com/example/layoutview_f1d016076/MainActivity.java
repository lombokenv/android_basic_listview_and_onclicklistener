package com.example.layoutview_f1d016076;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

public class MainActivity extends AppCompatActivity {
    ListView listView;
    String[] mhsNames = {"Rhomy Idris Sardi", "Mahasiswa 2", "Mahasiswa 3", "Mahasiswa 4"};
    String[] mhsNIM = {"F1D016076", "F1D016000","F1D016000","F1D016000"};
    String[] mhsDesc = {
            "Hai, Perkenalkan nama saya Romy. Saya adalah seorang mahasiswa T. Informatika Universitas " +
                    "Mataram. Saya adalah seorang developer WEB, dan sekarang saya sedang belajar " +
                    "tentang Android. Ini merupakan tugas Android Basic seri Layout, dimana aplikasi ini saya bangun " +
                    "untuk memenuhi salah satu tugas Pemgoraman Mobile menggunakan bahasa pemrograman Java " +
                    "dan memanfaatkan property dari ListView.",
            "2st lorem ipsum dolor sit amet",
            "3st lorem ipsum dolor sit amet",
            "4st lorem ipsum dolor sit amet"
    };
    int[] mhsProfil = {
            R.drawable.mark,
            R.drawable.stevejobs,
            R.drawable.linustrovald,
            R.drawable.billgates
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //to findding ListView in mainActivity
        listView = findViewById(R.id.listView);

        CustomAdapter customAdapter = new CustomAdapter();
        listView.setAdapter(customAdapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(
                        getApplicationContext(),
                        mhsNames[position],
                        Toast.LENGTH_LONG).show();
                int pos = position;
                Intent intent = new Intent(getApplicationContext(),item_activity.class);
                intent.putExtra("posisi",pos);
                intent.putExtra("name", mhsNames[position]);
                intent.putExtra("nim",mhsNIM[position]);
                intent.putExtra("desc",mhsDesc[position]);
                intent.putExtra("image", mhsProfil[position]);
                startActivity(intent);

            }

        });

    }

    private class CustomAdapter extends BaseAdapter{

        @Override
        public int getCount() {
            return mhsNames.length;
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View view1 = getLayoutInflater().inflate(R.layout.row_data, null);
            //getting view in row_data
            TextView mhsname = view1.findViewById(R.id.username);
            TextView mhsnim =view1.findViewById(R.id.nim);
            ImageView imageProfil = view1.findViewById(R.id.images);

            Intent intent2 = getIntent();
            String updateUser = intent2.getStringExtra("name");
            String updateNim = intent2.getStringExtra("nim");
            int forUpdateData = intent2.getIntExtra("pos",0);
            int param = intent2.getIntExtra("par",0);

            if(param == 1){
                mhsNames[forUpdateData] = updateUser;
                mhsNIM[forUpdateData] = updateNim;
            }

            //setting view in row_data
            mhsname.setText(mhsNames[position]);
            mhsnim.setText(mhsNIM[position]);
            imageProfil.setImageResource(mhsProfil[position]);

            return view1;
        }
    }
}
